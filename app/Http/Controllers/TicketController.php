<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Ticket;
use \App\Appointment;
use \App\Service;
use \App\Doctor;
use \App\User;
use Session;
use Auth;

class TicketController extends Controller
{
    public function ticket(){
    	$appointments = Appointment::all();
        $services = Service::all();
        $users = User::all();
        $doctors = Doctor::all();

        return view('userviews.appointments', compact('appointments', 'services', 'doctors', 'users'));
    }
}
