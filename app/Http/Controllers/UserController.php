<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\User;
use \App\Order;

class UserController extends Controller
{
    public function index(){
    	$users = User::all();

    	$usersWithOrders = $orders->pluck('user_id')->toArray();

    	// dd($usersWithOrders);

    	return view('adminviews.users', compact('users', 'usersWithOrders'));
    }

    public function changeRole($id){
    	$user = User::find($id);

    	if($user->role_id == 2){
    		$user->role_id = 1;
    	}else{
    		$user->role_id = 2;
    	}
    		$user->save();
    		return redirect('/allusers');
    }

    public function destroy($id){
    	$user = User::find($id);
    	$user->delete();

    	return redirect()->back();
    }

    public function ban($id){
        $user = User::find($id);
        $banned_until = now()->addMinutes(5);
        // dd($banned_until);
        $user->banned_until = $banned_until;
        $user->save();
        return redirect()->back();
    }
}
