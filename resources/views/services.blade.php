@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">Services</h1>
<div class="column">
    <div class="col-lg-12">
        <br />
        @if($message = Session::get('success'))
            <div class="alert alert-success">
                <h1 class="text-center">{{$message}}</h1>
            </div> 
        @endif           
    </div>
</div>

@if(Session::has("message"))
    <h4>{{Session::get('message')}}</h4>
@endif

<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <h4>Our Services</h4>
            <hr>
            @auth
                @if(Auth::user()->role_id==1)
            <a href="/addservice" class="btn btn-warning">Add Service</a>
            <hr>
                @endif
            @endauth

            <a href="/addappointment" class="btn btn-info">Schedule an Appointment Now!</a>
        </div>
        <div class="col-lg-10">
            <div class="row w-100">
                @foreach($services as $service)
                <div class="col-lg-4 p-3 my-2">
                    <div class="card">
                        <img class="card-img-top" src="{{asset($service->imgPath)}}" alt="Nothing" height="250px">
                        <div class="card-body">
                            <h2 class="card-title">{{$service->name}}</h2>
                            <p class="card-text">{{$service->description}}</p>
                            <p class="card-text">{{$service->duration}}</p>

                            @if($service->deleted_at != NULL)
                            <p class="text-danger">No Available Sessions</p>
                            @endif
                        </div>
                        
                        @auth
                         @if(Auth::user()->role_id==1)

                        <div class="card-footer d-flex flex-row justify-content-between ">
                             {{-- @if($service->deleted_at == NULL) --}}
                            <form action="/deleteservice/{{$service->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">DELETE</button>
                            </form>
                           {{-- @else --}}
                            <form action="/restoreservice/{{$service->id}}" method="POST">
                                @csrf
                                <button class="btn btn-info" type="submit">Restock</button>
                            </form>
                           {{-- @endif --}}
                            <a href="/editservice/{{$service->id}}" class="btn btn-success">Edit</a>
                        </div>
                         @endif
                        @endauth

                        {{-- @auth This will check if the user is already authenticated --}}
                            {{-- @if(Auth::user()->role_id==2) --}}
                            {{-- @endif --}}
                        {{-- @endauth --}}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection