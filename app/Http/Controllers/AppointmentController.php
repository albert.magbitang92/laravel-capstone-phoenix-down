<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Appointment;
use \App\Service;
use \App\Doctor;
use \App\User;
use Session;
use Auth;

class AppointmentController extends Controller
{
    public function index(){

        $appointments = Appointment::where("user_id",Auth::user()->id)->get();
        $services = Service::all();
        $users = User::all();
        $doctors = Doctor::all();

        $x=Appointment::where("user_id",Auth::user()->id)->pluck("service_id")->all();

        $services=Service::whereIn("id",$x)->get();


        return view('userviews.appointments', compact('appointments', 'services', 'doctors', 'users'));
    }


    public function serviceFilter($id){
        $services = Service::where($id)->get();

        return view('addappointment', compact('services'));
    }

    public function doctorFilter($id){
        $doctors = Doctor::where($id)->get();

        return view('addappointment', compact('doctors'));
    }

    public function create(){
        $users = User::all();
        $services = Service::all();
        $appointments = Appointment::all();
        $doctors = Doctor::all();

        return view('userviews.addappointment', compact('services', 'users', 'appointments', 'doctors'));
    }

    public function store(Request $req){
        // validate
        $users = User::all();
        $services = Service::all();
        $appointments = Appointment::all();
        $doctors = Doctor::all();
        // $rules = array(
        //     "date" => "required|date",
        //     "time" => "required"
        // );

        // $this->validate($req, $rules);

        // capture
        $newAppointment = new Appointment;
        $newAppointment->service_id = $req->service_id;
        $newAppointment->doctor_id = $req->doctor_id;
        $newAppointment->user_id = Auth::user()->id;
        $newAppointment->date = $req->date;
        $newAppointment->time = $req->time;

        // dd($newAppointment);

        // save 
        $newAppointment->save();
        return redirect('appointments');
    }

    public function edit($id){
        $appointment = Appointment::find($id);
        $service = Service::find($id);
        $doctor = Doctor::find($id);

        return view('userviews.editappointment', compact('appointment', 'service'));
    }

    public function update($id, Request $req){
        $appointment = Appointment::find($id);

        // $rules = array(
        //     "date" => "required",
        //     "time" => "required"
        // );

        // $this->validate($req, $rules);

        $appointment->date = $req->date;
        $appointment->time = $req->time;
        

        dd($appointment);

        $appointment->save();
        return redirect('/appointments');
    }

    public function hakai($id){
        $appointmentToDelete = Appointment::find($id);
        $appointmentToDelete->delete();

        return redirect()->back()->with('success', 'Data Deleted');
    }

    public function ticket(){

        $users = User::all();
        $services = Service::all();
        $appointments = Appointment::all();
        $doctors = Doctor::all();

        return view('userviews.ticketfinalize', compact('services', 'users', 'appointments', 'doctors'));
    }

    public function clearAppointment(){
        Session::forget('appointments');
        return redirect()->back()->with('success', 'Ticket is Now Rendered Null. Thanks to you, I do not get to eat this month');
    }
}