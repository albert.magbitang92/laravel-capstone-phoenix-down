<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function services(){
    	return $this->belongsTo("\App\Service");
    }

    // public function orders(){
    // 	return $this->belongsToMany("\App\Service")->withPivot("quantity")->withTimeStamps();
    // }
}
