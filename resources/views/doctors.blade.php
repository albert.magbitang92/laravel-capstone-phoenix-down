@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">Meet our Team</h1>
<div class="container">
    <div class="row">
        <div class="col-lg-2">
            @auth
                @if(Auth::user()->role_id==1)
            <a href="/adddoctor" class="btn btn-warning">Add Doctor</a>
                @endif
            @endauth
        </div>
        <div class="col-lg-10">
            <div class="row w-100">
                @foreach($doctors as $doctor)
                <div class="col-lg-4 p-3 my-2">
                    <div class="card">
                        <img class="card-img-top rounded-circle" src="{{asset($doctor->imgPath)}}" alt="Nothing" height="300px" >
                        <div class="card-body">
                            <h3 class="card-title">{{$doctor->fName}} {{$doctor->lName}}</h3>
                            <p class="card-text">{{$doctor->introduction}}</p>
                            <p class="card-text">{{$doctor->idLicense}}</p>
                        </div>
                        
                        @auth 
                        {{-- This will check if the user is already authenticated --}}
                         @if(Auth::user()->role_id==1)

                        <div class="card-footer d-flex flex-row justify-content-around">
                             {{-- @if($doctor->deleted_at == NULL) --}}
                            <form action="/deletedoctor/{{$doctor->id}}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger" type="submit">TERMINATE</button>
                            </form>
                           {{-- @else --}}
                            <form action="/restoredoctor/{{$doctor->id}}" method="POST">
                                @csrf
                                <button class="btn btn-info" type="submit">Return</button>
                            </form>
                           {{-- @endif --}}
                            <a href="/editdoctor/{{$doctor->id}}" class="btn btn-success">Edit</a>
                        </div>
                         @endif
                        @endauth
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection