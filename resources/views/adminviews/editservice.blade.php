@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Edit Item Form</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/editservice/{{$service->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="name">Name</label>
			<input type="text" name="name" class="form-control" value="{{$service->name}}">
		</div>
		<div class="form-group">
			<label for="description">Description</label>
			<input type="textarea" name="description" class="form-control" value="{{$service->description}}">
		</div>
		<div class="form-group">
			<label for="duration">Duration</label>
			<input type="text" name="duration" class="form-control" value="{{$service->duration}}">
		</div>
		<div class="form-group">
			<label for="imgPath">Image:</label>
			<input type="file" name="imgPath" class="form-control">
		</div>
		
		<button class="btn btn-warning" type="submit">Edit Service</button>
	</form>
</div>
@endsection