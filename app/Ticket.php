<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public function user(){
    	return $this->belongsTo("\App\User");
    }

    public function status(){
    	return $this->belongsToMany("\App\Status");
    }

    public function appointment(){
    	return $this->belongsToMany("\App\Appointment");
    }

    public function service(){
    	return $this->belongsTo("\App\Service");
    }

    public function doctor(){
    	return $this->belongsTo("\App\Doctor");
    }
}
