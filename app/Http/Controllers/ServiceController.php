<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Service;
use Session;

class ServiceController extends Controller
{
    public function index(){
        $services = Service::all();

        return view('services', compact('services'));
    }

    // public function show($id){
    //     return view('services', ['id' => $id]);
    // }

    public function filter($id){
        $services = Service::where($id)->get();


        return view('services', compact('services'));
    }

    public function search(Request $req){
        $services = Service::where('name', 'LIKE', '%' . $req->search . '%')->orWhere('description','LIKE', '%' . $req->search . '%' )->get();

        if(count($services)==0){
            Session::flash('message', 'Service not found');
        }
    }

    public function create(){
        $services = Service::all();

        return view('adminviews.addservice', compact('services'));
    }

    public function store(Request $req){
        // validate
        $rules = array(
            "name" => "required",
            "description" => "required",
            "duration" => "required",
            "imgPath" => "required|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap"
        );

        $this->validate($req, $rules);

        // capture
        $newService = new Service;
        $newService->name = $req->name;
        $newService->description = $req->description;
        $newService->duration = $req->duration;

        // image handling
        $image = $req->file('imgPath');
        // We'll rename the image
        $image_name = time().".".$image->getClientOriginalExtension();

        $destination = "images/"; //corresponds to the public images directory

        $image->move($destination, $image_name);

        $newService->imgPath = $destination.$image_name;

        // save 
        $newService->save();
        return redirect('/services')->with('success', 'Data Added');
    }

    public function edit($id){
        $service = Service::find($id);

        return view('adminviews.editservice', compact('service'));
    }

    public function update($id, Request $req){
        $service = Service::find($id);

        $rules = array(
            "name" => "required",
            "description" => "required",
            "duration" => "required",
            "imgPath" => "image|mimes:jpeg,jpg,png,gif,tiff,tif,bitmap,webP"
        );

        $this->validate($req, $rules);

        $service->name = $req->name;
        $service->description = $req->description;
        $service->duration = $req->duration;

        if($req->file('imgPath') != null){
            $image = $req->file('imgPath');
            $image_name = time(). ".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $service->imgPath = $destination.$image_name;
        }

        $service->save();
        return redirect('/services')->with('success', 'Data Edited');
    }

    public function destroy($id){
        $serviceToDelete = Service::find($id);
        $serviceToDelete->delete();

        return redirect()->back()->with('success', 'Data Deleted');
    }
}
