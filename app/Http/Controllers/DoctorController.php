<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Doctor;
use \App\Appointment;
use Session;

class DoctorController extends Controller
{
    public function index(){
        $doctors = Doctor::all();

        return view('doctors', compact('doctors'));
    }

    public function create(){
        $doctors = Doctor::all();

        return view('adminviews.adddoctor', compact('doctors'));
    }

    public function store(Request $req){
        // validate
        $rules = array(
            "fname" => "required",
            "lname" => "required",
            "idLicense" => "required",
            "introduction" => "required",
            "imgPath" => "required|image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap"
        );

        $this->validate($req, $rules);

        // capture
        $newDoctor = new Doctor;
        $newDoctor->fname = $req->fname;
        $newDoctor->lname = $req->lname;
        $newDoctor->idLicense = $req->idLicense;
        $newDoctor->introduction = $req->introduction;
        
        // image handling
        $image = $req->file('imgPath');
        // We'll rename the image
        $image_name = time().".".$image->getClientOriginalExtension();

        $destination = "images/"; //corresponds to the public images directory

        $image->move($destination, $image_name);

        $newDoctor->imgPath = $destination.$image_name;

        // save 
        $newDoctor->save();
        return redirect('/doctors');
    }

    public function edit($id){
        $doctor = Doctor::find($id);

        return view('adminviews.editdoctor', compact('doctor'));
    }

    public function update($id, Request $req){
        $doctor = Doctor::find($id);

        $rules = array(
            "fname" => "required",
            "lname" => "required",
            "idLicense" => "required",
            "introduction" => "required",
            "imgPath" => "image|mimes:jpeg,jpg,png,gif,tiff,tif,webp,bitmap"
        );

        $this->validate($req, $rules);

        $doctor->fname = $req->fname;
        $doctor->lname = $req->lname;
        $doctor->idLicense = $req->idLicense;
        $doctor->introduction = $req->introduction;

        if($req->file('imgPath') != null){
            $image = $req->file('imgPath');
            $image_name = time(). ".".$image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $image_name);
            $doctor->imgPath = $destination.$image_name;
        }

        $doctor->save();
        return redirect('/doctors');
    }

    public function destroy($id){
        $doctorToDelete = Doctor::find($id);
        $doctorToDelete->delete();

        return redirect()->back();
    }
}
