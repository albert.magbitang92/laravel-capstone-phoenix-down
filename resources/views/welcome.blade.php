@extends('layouts.layout')

@section('content')
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <img src="/images/banner.png" alt="Phoenix Down Emblem">
                <div class="title m-b-md">
                   Raises You Up When You're Down
                   <p id="note">*Note: Cannot Revive Dead People, Sorry.</p>
                </div>
            </div>
        </div>
@endsection