@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Service Ticket Draft</h1>

{{-- @if($items != null) --}}
<div class="column">
    <div class="col-lg-12">
        <br />
        @if($message = Session::get('success'))
            <div class="alert alert-danger">
                <h1 class="text-center">{{$message}}</h1>
            </div> 
        @endif           
    </div>
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<a href="/services" class="btn btn-primary">+ Add More Sessions</a>
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Client Name:</th>
						<th>Service Name:</th>
						<th>Date:</th>
						<th>Time:</th>
						<th>Appointed Doctor:</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{{-- should be from the id of the user that's currently logged in --}}
					{{-- @foreach($users as $user) --}}
					{{-- the data here should be from the addappointments/blade --}}
					{{-- @foreach($services as $service) --}}
					@foreach($appointments as $appointment)
					{{-- should be blank at first when user enters appointments.blade.php --}}
					{{-- should be from appointments_to_doctors --}}
					{{-- @foreach($doctors as $doctor) --}}
					<tr>
						<td>{{$appointment->user->name}}</td>
						<td>{{$appointment->service->name}}</td>
						<td>{{$appointment->date}}</td>
						<td>{{$appointment->time}}</td>
						<td>Dr.{{$appointment->doctor->fname}}, {{$appointment->doctor->lname}}</td>
						
						{{-- <td><a href="/choosedoctors" class="btn btn-info">Choose Doctor</a></td> --}}
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><a href="/editappointment/{{$appointment->id}}" class="btn btn-warning">Edit Appointment</a></td>
						<td><form action="/deleteappointment/{{$appointment->id}}" method="POST">
								@csrf
								@method('DELETE')
								<button class="btn btn-danger" type="submit">Cancel Appointment</button>
							</form>
						</td>
						<td></td>
					</tr>
					{{-- @endforeach
					@endforeach
					@endforeach --}}
					@endforeach
					<tr>
						<td></td>
						<td></td>
						<td>
							<a href="/clearAppointment" class="btn btn-dark">Clear Appointments</a>
						</td>
						<td></td>
						<td>
							<a class="btn btn-success" href="/ticketfinalize" ">Everything's OK Now.</href>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
{{-- @else

	<h2 class="text-center py-5">Ticket is Now Rendered Null (Thanks to you, I don't get to eat this month). Nothing to do here...</h2> --}}

{{-- @endif --}}

@endsection