@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Edit Your Appointment</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/editappointment/{{$appointment->id}}" method="POST" enctype="multipart/form-data">
		@csrf
		@method('PATCH')
		<div class="form-group">
			<label for="date">New Date</label>
			<input type="date" name="date" class="form-control">
		</div>
		<div class="form-group">
			<label for="time">New Time</label>
			<input type="time" name="time" class="form-control">
		</div>
		<button class="btn btn-warning" type="submit">Submit Preferred Appointment</button>
	</form>
</div>
@endsection