@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">List of Available Therapists</h1>
<div class="container">
    <div class="row">
        <div class="col-lg-10">
            <div class="row w-100">
                @foreach($doctors as $doctor)
                <div class="col-lg-4 p-3 my-2">
                    <div class="card">
                        <img class="card-img rounded-circle" src="{{asset($doctor->imgPath)}}" alt="Nothing" height="300px" >
                        <div class="card-body">
                            <h3 class="card-title">{{$doctor->fName}} {{$doctor->lName}}</h3>
                            <p class="card-text">{{$doctor->introduction}}</p>
                            <p class="card-text">{{$doctor->idLicense}}</p>
                        </div>
                        
                        {{-- @auth This will check if the user is already authenticated --}}
                         {{-- @if(Auth::user()->role_id==2) --}}

                        <div class="card-footer d-flex flex-row justify-content-around">
                            <form action='/choosedoctors' method="POST">
                                @csrf
                                @method('PATCH')
                                <button class="btn btn-info" type="submit">Pick this guy</button>
                            </form>
                        </div>      
                         {{-- @endif --}}
                        {{-- @endauth --}}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

@endsection