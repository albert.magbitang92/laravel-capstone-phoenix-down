<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});
Auth::routes();


// Landing Page, and Post Log-in
Route::get('/home', 'HomeController@index')->name('welcome');

// Admin Control
// Route::middleware("admin")->group(function(){

// Services - Basic
Route::get('/services', 'ServiceController@index');
Route::get('/services/{id}', 'ServiceController@filter');
Route::get('/services/sort/{sort}', 'ServiceController@sort');
Route::post('/services', 'ServiceController@search');

// Services - Adding File
Route::get('/addservice', 'ServiceController@create');
Route::post('/addservice', 'ServiceController@store');

// Services - Editing File
Route::get('/editservice/{id}', 'ServiceController@edit');
Route::patch('/editservice/{id}', 'ServiceController@update');

// Services - Deleting File
Route::delete('/deleteservice/{id}', 'ServiceController@destroy');

// Doctors - Basic
Route::get('/doctors', 'DoctorController@index');

// Doctors - Adding Doctor
Route::get('/adddoctor', 'DoctorController@create');
Route::post('/adddoctor', 'DoctorController@store');

// Doctors - Edit Doctor
Route::get('/editdoctor/{id}', 'DoctorController@edit');
Route::patch('/editdoctor/{id}', 'DoctorController@update');

//Doctors - Delete Doctor
Route::delete('/deletedoctor/{id}', 'DoctorController@destroy');

// Users - Basic
Route::get('/allusers', 'UserController@index');

// Users - Edit
Route::get('/changerole/{id}', 'UserController@changeRole');
Route::delete('/deleteuser/{id}', 'UserController@destroy');
Route::get('/ban/{id}', 'UserController@ban');

//Users Control
// Route::middleware("user")->group(function(){

// Appointments - After Service Button.
Route::get('/appointments', 'AppointmentController@index');

//Add Appointments
Route::get('/addappointment', 'AppointmentController@create');
Route::post('/addappointment', 'AppointmentController@store');

//Edit Appointments
Route::get('/editappointment/{id}', 'AppointmentController@edit');
Route::patch('/editappointment/{id}', 'AppointmentController@update');

//Show Appointment & Register Only According to User or Author
Route::get('/ticketfinalize', 'AppointmentController@ticket');
//Delete Appointments
Route::delete('/deleteappointment/{id}', 'AppointmentController@hakai');
Route::get('/clearAppointment', "AppointmentController@clearAppointment");

//Choose Doctors - Idk, man. I'm confused as hell. Better ask for advice on this one.