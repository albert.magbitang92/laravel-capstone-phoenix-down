@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Add New Doctor</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/adddoctor" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="fname">First Name</label>
			<input type="text" name="fname" class="form-control">
		</div>
		<div class="form-group">
			<label for="lname">Last Name</label>
			<input type="text" name="lname" class="form-control">
		</div>
		<div class="form-group">
			<label for="introduction">Introduction</label>
			<input type="textarea" name="introduction" class="form-control">
		</div>
		<div class="form-group">
			<label for="idLicense">PRC License Number</label>
			<input type="text" name="idLicense" class="form-control">
		</div>
		<div class="form-group">
			<label for="imgPath">Image</label>
			<input type="file" name="imgPath" class="form-control">
		</div>
		<button class="btn btn-warning" type="submit">Add Doctor</button>
	</form>
</div>
@endsection