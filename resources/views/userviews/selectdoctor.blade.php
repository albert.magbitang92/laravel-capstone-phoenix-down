@extends("layouts.app")
@section('content')

<h1 class="text-center py-5">Meet our Team</h1>
<div class="container">
    <div class="row">
        <div class="col-lg-2">
            <a href="/selectdoctor" class="btn btn-warning">Choose Your Therapist</a>
        </div>
        <div class="col-lg-10">
            {{-- @if(doctor()->status_id== 2||3
                then, hide doctor ??? Ask Ms. Cris as to how this works --}}
            <div class="row w-100">
                @foreach($doctors as $doctor)
                <div class="col-lg-4 p-3 my-2">
                    <div class="card">
                        <img class="card-img-top rounded-circle" src="{{asset($doctor->imgPath)}}" alt="Nothing" height="300px" >
                        <div class="card-body">
                            <h3 class="card-title">{{$doctor->fName}} {{$doctor->lName}}</h3>
                            <p class="card-text">{{$doctor->introduction}}</p>
                            <p class="card-text">{{$doctor->idLicense}}</p>
                        </div>
                        
                        {{-- @auth This will check if the user is already authenticated --}}
                         {{-- @if(Auth::user()->role_id==1) --}}

                        <div class="card-footer d-flex flex-row justify-content-around">
                             {{-- @if($doctor->deleted_at == NULL) --}}
                           <form action="/choosedoctor/{{$doctor->id}}" method="POST">
                                @csrf
                            <button class="btn btn-success" onclick="addDocToSched({{$doctor->id}}, `{{$doctor->name}}`)" type="submit">Add to Scheduled Session</button>
                            {{-- </form> --}}
                        </div>
                         {{-- @endif --}}
                        {{-- @endauth --}}
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    const addToCart = (id, doctorName) =>{
        console.log(doctorName)
        alert("You appointment with Dr." + doctorName + " has been scheduled");

        // let data = new FormData;

        // data.append("_token", "{{ csrf_token() }}");
        // data.append("quantity", quantity);

        fetch("/scheduledsession/"+id,{
            method: "POST",
            body: {
                "_token": {{csrf_token()}},
                "doctorName": doctorName
            }
        }).then(res=>res.text())
        .then(res=>console.log(res))
    }

</script>

@endsection