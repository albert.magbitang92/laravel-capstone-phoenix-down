@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Add New Appointment</h1>

<div class="col-lg-4 offset-lg-4">
	<form action="/addappointment" method="POST" enctype="multipart/form-data">
		@csrf
		<div class="form-group">
			<label for="sName"><u>Desired Service</u></label>
			<br>
			<select id="serviceFilter" onchange="serviceFilter()" name="service_id">	
			@foreach($services as $service)
	            <option value="{{$service->id}}">{{$service->name}}</option>
	        @endforeach
	        </select>
		</div>
		<div class="form-group">
			<label for="date">Preferred Date</label>
			<input type="date" name="date" class="form-control">
		</div>
		<div class="form-group">
			<label for="time">Preferred Time</label>
			<input type="time" name="time" class="form-control">
		</div>
		<div class="form-group">
			<label for="sName"><u>Chosen Therapist</u></label>
			<br>
			<select id="doctorFilter" onchange="doctorFilter()" name="doctor_id">	
			@foreach($doctors as $doctor)
	            <option value="{{$doctor->id}}">{{$doctor->fname}} {{$doctor->lname}}</option>
	        @endforeach
	        </select>
		</div>
		<button class="btn btn-warning" type="submit">Submit Preferred Appointment</button>
	</form>
</div>
@endsection