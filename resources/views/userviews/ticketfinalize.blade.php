@extends('layouts.app')
@section('content')

<h1 class="text-center py-5">Final Service Ticket Submitted!</h1>

{{-- @if($items != null) --}}
<div class="container">
	<div class="row">
		<div class="col-lg-8 offset-lg-2">
			<table class="table table-striped">
				<thead>
					<tr>
						<th>Client Name:</th>
						<th>Service Name:</th>
						<th>Date:</th>
						<th>Time:</th>
						<th>Appointed Doctor:</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{{-- should be from the id of the user that's currently logged in --}}
					{{-- @foreach($users as $user) --}}
					{{-- the data here should be from the addappointments/blade --}}
					{{-- @foreach($services as $service) --}}
					@foreach($appointments as $appointment)
					{{-- should be blank at first when user enters appointments.blade.php --}}
					{{-- should be from appointments_to_doctors --}}
					{{-- @foreach($doctors as $doctor) --}}
					<tr>
						<td>{{$appointment->user->name}}</td>
						<td>{{$appointment->service->name}}</td>
						<td>{{$appointment->date}}</td>
						<td>{{$appointment->time}}</td>
						<td>Dr.{{$appointment->doctor->fname}}, {{$appointment->doctor->lname}}</td>
					</tr>
					{{-- @endforeach
					@endforeach
					@endforeach --}}
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
{{-- @else

	<h2 class="text-center py-5">Ticket is Now Rendered Null (Thanks to you, I don't get to eat this month). Nothing to do here...</h2> --}}

{{-- @endif --}}

@endsection